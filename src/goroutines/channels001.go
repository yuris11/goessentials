package goroutines

import "fmt"

func Channels001() {
	ch := make(chan string)
	go f001(ch)
	for s := range ch {
		fmt.Println(s)
	}
	fmt.Println("=end=")
}

func f001(ch chan string) {
	defer close(ch) //!!!!!!
	ch <- "str1"
	ch <- "str2"
	ch <- "str3"
	ch <- "str4"
}
