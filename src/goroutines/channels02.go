package goroutines

import (
	"fmt"
	"time"
)

type tdata struct {
	a int
	s string
}

//Channels2 ...
func Channels02() {
	fmt.Println("----------goroutines - channels-------------")
	ch := make(chan int)
	data := tdata{a: 0, s: "mystr"}
	for i := 0; i < 5; i++ {
		go changeData(i, &data, ch)
		c := <-ch
		fmt.Printf("%v %v\n", data, c)
	}
}

func changeData(n int, pdata *tdata, ch chan int) {
	pdata.a++
	time.Sleep(time.Second)
	ch <- pdata.a
}
