package goroutines

import (
	"fmt"
	"sync"
)

var waitg sync.WaitGroup

//Conveyuor ...
func Conveyuor() {
	fmt.Println("---------- goroutines - conveyuor -------------")
	waitg.Add(3)
	c0 := make(chan int)
	c1 := make(chan int)
	go sourceFn(c0)
	go middleFn(c0, c1)
	go lastFn(c1)
	waitg.Wait()
}

func sourceFn(c0 chan int) {
	defer waitg.Done()
	fmt.Println("--1--")
	s := []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}
	for _, v := range s {
		c0 <- v
		//time.Sleep(time.Millisecond * 200)
	}
	close(c0)
}

// func deferFn(c chan int) {
// 	close(c)
// 	waitg.Done()
// }

func middleFn(c0 chan int, c1 chan int) {
	defer waitg.Done()
	fmt.Println("--2--")
	for v := range c0 {
		c1 <- v * v
	}
	close(c1)
}

func lastFn(c1 chan int) {
	defer waitg.Done()
	fmt.Println("--3--")
	for v := range c1 {
		fmt.Printf("*%d ", v)
	}
}
