package goroutines

import (
	"fmt"
	"sync"
	"time"
)

var wg sync.WaitGroup
var mutex sync.Mutex
var data int

const ng = 5

//Mutex ...
func Mutex() {
	fmt.Println("----------goroutines - Mutex - WaitGroup -------------")
	wg.Add(ng)
	for i := 0; i < ng; i++ {
		go work(i)
	}
	fmt.Println("===Wait===")
	wg.Wait()
	fmt.Println("===End===")
}

func work(n int) {
	mutex.Lock()
	fmt.Printf("Goroutine=%d  ", n+1)
	data = 0
	for i := 0; i < 10; i++ {
		time.Sleep(10 * time.Millisecond)
		data++
		fmt.Printf("%d ", data)
	}
	fmt.Printf("\n")
	mutex.Unlock()
	defer wg.Done()
}
