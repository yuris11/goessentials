package goroutines

import "fmt"

//Factorial1 ...
func Factorial1() {
	fmt.Println("----------goroutines - Factorial1-------------")
	intCh := make(chan int)
	go factorial1(7, intCh)
	for num := range intCh {
		fmt.Println(num)
	}
}

func factorial1(n int, ch chan int) {
	defer close(ch)
	result := 1
	for i := 1; i <= n; i++ {
		result *= i
		ch <- result
	}
}
