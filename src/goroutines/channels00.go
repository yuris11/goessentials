package goroutines

import (
	"fmt"
)

//Channels0 ...
func Channels00() {
	ch := make(chan int)
	go f(ch)
	ch <- 111 //wait!!!
	fmt.Println("=end=")
}

func f(ch chan int) {
	fmt.Println(<-ch)
}
