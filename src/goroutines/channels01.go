package goroutines

import (
	"fmt"
	"sync"
	//"time"
)

var wgr sync.WaitGroup

//Channels1 ...
func Channels01() {
	wgr.Add(1)
	c := make(chan int)
	fmt.Println("----------goroutines - channels1-------------")
	go printfn(c)
	for i := 1; i < 10; i++ {
		c <- i
	}
	close(c)
	wgr.Wait()
}

func printfn(c chan int) {
	defer wgr.Done()
	for {
		v, ok := <-c
		if ok {
			fmt.Println(v)
			//time.Sleep(time.Millisecond * 20)
		} else {
			fmt.Println("*** end ***")
			return
		}
	}
	/*for v := range c {
		fmt.Println(v)
		time.Sleep(time.Millisecond * 20)
	}
	fmt.Println("*** end ***")*/
}
