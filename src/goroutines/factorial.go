package goroutines

import "fmt"

//Factorial ...
func Factorial() {
	fmt.Println("----------goroutines - Factorial-------------")
	results := make(map[int]int)
	structCh := make(chan struct{})
	go factorial(5, structCh, results)
	c1 := <-structCh // ожидаем закрытия канала structCh - получаем данные ПО УМОЛЧАНИЮ после закрытия канала
	c2 := <-structCh //
	c3 := <-structCh //
	fmt.Println("Данные ПО УМОЛЧАНИЮ:", c1, c2, c3)

	for i, v := range results {
		fmt.Println(i, " - ", v)
	}
}

func factorial(n int, ch chan struct{}, results map[int]int) {
	defer close(ch) // закрываем канал после завершения горутины
	result := 1
	for i := 1; i <= n; i++ {
		result *= i
		results[i] = result
	}
}
