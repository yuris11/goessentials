package context1

import (
	"context"
	"fmt"
	"time"
)

//Cont1 ...
func Cont1() {
	fmt.Println("---------- context1 -------------")
	ctx := context.Background()
	ctxCancel, cancelFn := context.WithCancel(ctx)
	go task11(ctxCancel)
	time.Sleep(time.Second * 6)
	cancelFn()
	time.Sleep(time.Second * 1)
}

func task11(ctx context.Context) {
	i := 0
	for {
		select {
		case <-ctx.Done():
			fmt.Println("Normal exit")
			fmt.Println(ctx.Err())
			return
		default:
			fmt.Println(i)
			i++
			time.Sleep(time.Millisecond * 500)
		}
	}
}
