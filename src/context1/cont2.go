package context1

import (
	"context"
	"fmt"
	"time"
)

//Cont2 ...
func Cont2() {
	fmt.Println("---------- context1 -------------")
	fmt.Println("-START-")
	ctx, cancelFn := context.WithTimeout(context.Background(), time.Second*9)
	defer func() {
		cancelFn()
		fmt.Println("\n-ExitMain-")
	}()
	go task12(ctx)
	fmt.Println("-END-")
	time.Sleep(time.Second * 10)
}

func task12(ctx context.Context) {
	//after := time.After(time.Millisecond * 1000)
	ticker := time.NewTicker(time.Millisecond * 1000)
	for {
		select {
		case <-ctx.Done():
			fmt.Printf("\n-ExitContext-")
			return
		case <-ticker.C:
			fmt.Printf("%c", '*')
		default:
			time.Sleep(time.Millisecond * 100)
			fmt.Printf("%c", '.')
		}
	}
}
