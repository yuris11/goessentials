package slices

import "fmt"

//Slices ...
func Slices() {
	fmt.Println("----------slices-------------")
	arr := [10]int{0, 1, 2, 3, 4, 5, 6, 7, 8}
	fmt.Println("array:", arr)
	var slice0 = arr[2:5]
	fmt.Println("slice:", slice0)

	var slice1 = []int{1, 2, 3}
	fmt.Println("slice1:", slice1)

	slice2 := []string{"str1", "str2", "str3", "str4"}
	fmt.Println("slice2:", slice2)

	var slice3 []int = make([]int, 0, 50)
	slice3 = append(slice3, 11)
	slice3 = append(slice3, 22)
	slice3 = append(slice3, 33)
	slice3 = append(slice3, 44)
	fmt.Printf("slice3:%v length:%d caption:%d\n", slice3, len(slice3), cap(slice3))
	fmt.Print("slice3 data: ")
	for _, v := range slice3 {
		fmt.Printf("%d ", v)
	}

	slice4 := []int{1, 2, 3}
	slice5 := []int{4, 5, 6}
	fmt.Printf("\nslice4:%v slice5:%v v=%d\n", slice4, slice5,slice5[2])
	sliceAdd := append(slice4, slice5...)
	fmt.Println("slice_add:", sliceAdd)
}
