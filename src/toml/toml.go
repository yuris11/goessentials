package toml

import (
	"fmt"
	"log"
	"github.com/BurntSushi/toml"
)

const tomlFilePath="src/toml/example1.toml"

type type_info struct {
	Name string
	Age int
}

type type_values struct {
	Data1 int
	List []float64
}

type type_servers struct {
	IP string
	Port int
}


type tomlData struct {
	Sorokin type_info
	Moshkin type_info 
	Values type_values
	Servers map[string]type_servers
}

func Toml() {
	fmt.Println("----------toml-------------")
	var tdata tomlData
 	if _,err := toml.DecodeFile(tomlFilePath,&tdata); err!=nil { 
		log.Fatal()
	}
	fmt.Println(tdata)
}

