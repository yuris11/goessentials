package interfaces

import (
	"fmt"
	"reflect"
)

//InterfEmpty ...
func InterfEmpty() {
	fmt.Println("---------- interfaces - InterfEmpty -------------")
	myMap := map[string]interface{}{}
	myMap["one"] = 11
	myMap["two"] = "str22"
	myMap["three"] = float32(33.123)
	myMap["four"] = []int{1, 2, 3, 4}
	myMap["five"] = []float32{1.1, 2.2, 3.3, 4.4}
	fmt.Printf("%v\n", myMap)
	for ind, v := range myMap {
		fmt.Printf("%s=%v\t", ind, v)
		switch v.(type) {
		case int:
			fmt.Printf("%s\n", "type=int")
		case []int:
			fmt.Printf("%s\n", "type=[]int")
		case float32:
			fmt.Printf("%s\n", "type=float32")
		case string:
			fmt.Printf("%s\n", "type=string")
		default:
			fmt.Printf("reflect:%v\n", reflect.TypeOf(v))
		}
	}
	fmt.Println("")
	a := 3.1415
	fmt.Printf("type_a:%v\n", reflect.TypeOf(a))
}
