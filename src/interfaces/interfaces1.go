package interfaces

import "fmt"

//Logger ...
type Logger interface {
	Log(Informer)
}

//Log ...
func Log(p Informer) {
	fmt.Printf("Log:%v\t", p)
	p.Info()
}

//Informer ...
type Informer interface {
	Info()
}

type tman struct {
	name string
	age  int
}

type tbook struct {
	title string
	size  int
}

func (p tman) Info() {
	fmt.Printf("Man:%s age=%d\n", p.name, p.age)
}

func (p tbook) Info() {
	fmt.Printf("Book:%s %d\n", p.title, p.size)
}

func info(person Informer) {
	person.Info()
}

//Interfaces ...
func Interfaces() {
	fmt.Println("----------interfaces-------------")
	man := tman{"Юрий", 62}
	book := tbook{"Мёртвые души", 544}

	info(man)
	info(book)

	fmt.Println("")

	arr := []Informer{man, book}
	for _, v := range arr {
		info(v)
	}

	fmt.Println("")

	Log(man)
	Log(book)
}
