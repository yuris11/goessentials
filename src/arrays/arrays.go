package arrays

import "fmt"

//Arrays ...
func Arrays() {
	fmt.Println("----------arrays-------------")
	var arr1 [5]int
	arr1[0] = 1
	arr1[1] = 2
	arr1[2] = 3
	fmt.Println("arr1=", arr1)

	arr2 := [3]float32{2.31, 4.51}
	fmt.Println("arr2=", arr2)

	arr3 := [...]int{1, 2, 3, 4, 5}
	fmt.Println("arr3=", arr3, "len=", len(arr3))

	for i, v := range arr3 {
		fmt.Printf("index=%d v=%d\n", i, v)
	}
}
