package time1

import (
	"fmt"
	"time"
)

//Time1 ...
func Time1() {
	fmt.Println("----------time1-------------")
	t1 := time.RFC3339
	t2 := time.RFC822Z
	t3 := time.RFC850
	t4 := "2006-01-02"
	t5 := "День:02 Месяц:01 Год:06 15:04:05"
	t6 := "02-01-2006 15:04:05 January Monday" //dd-mm-yyyy hh:mm:ss
	t := time.Now()
	fmt.Println("Default format:", t)
	fmt.Println(t.Format(t1))
	fmt.Println(t.Format(t2))
	fmt.Println(t.Format(t3))
	fmt.Println(t.Format(t4))
	fmt.Println(t.Format(t5))
	fmt.Println(t.Format(t6))
	fmt.Println(t.Date())

	//tt, err := time.Parse(t4, "2021-02-28")
	tt, err := time.Parse(t5, "День:20 Месяц:12 Год:21 19:21:22")
	if err != nil {
		panic(err)
	}
	fmt.Print("Parsed date: ")
	fmt.Println(tt)
}
