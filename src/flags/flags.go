package flags

import "fmt"
import "flag"


//var strPrm string
//var intPrm int

func Flags() {
	fmt.Println("----------flags-------------")
	strPrmPtr:=flag.String("strParam", "stringDefault", "strParam (string)")
	intPrmPtr:=flag.Int("flagParam", 0, "flagParam (default 0)")
	bPtr:=flag.Bool("b", false, "boolean flag")
	flag.Parse()
	fmt.Printf("strPrm=%v, intPrm=%v boolFlag=%v\n",*strPrmPtr,*intPrmPtr,*bPtr)
}

