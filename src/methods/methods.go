package methods

import "fmt"

type person struct {
	name string
	age  int
}

func (p *person) changeAge(newAge int) {
	p.age = newAge
}

func (p *person) changeName(newName string) {
	p.name = newName
}

//Methods ...
func Methods() {
	fmt.Println("----------methods-------------")
	pers := person{"Yuris", 62}
	fmt.Println("old:", pers)
	pers.changeAge(52)
	pers.changeName("Alex")
	fmt.Println("new:", pers)
}
