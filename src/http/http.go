package http

import (
	"fmt"
	"net/http"
)

func Http() {
	fmt.Println("----------http-------------")
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		fmt.Println(r.URL)
		fmt.Fprintf(w, "<h1>Hello World!!!</h1>")
	})
	http.ListenAndServe(":5000", nil)

}
