package strings

import (
	"fmt"
	"strings"
	"unicode/utf8"
)

//Strings ...
func Strings() {
	fmt.Println("----------strings-------------")
	var str = "Это исходная строка"
	fmt.Printf("Source string: %s\n", str)
	fmt.Printf("ToUpper: %s\n", strings.ToUpper(str))
	strFields := strings.Fields(str)
	fmt.Printf("Fields:%v, n=%d\n", strFields, len(strFields))
	fmt.Printf("ReplaceAll:%s\n", strings.ReplaceAll(str, "сх", "СХ."))
	fmt.Printf("len(bytes)=%d len(runes)=%d\n\n", len(str), utf8.RuneCountInString(str))
	fmt.Println("Iterate over string (correct way, for-range):")
	for i, v := range str { //iterate
		fmt.Printf("(%d)%c ", i, v)
	}
	fmt.Printf("\n\n")

	type MyR []rune
	type MyB []byte
	myrunes := MyR(str)
	mybytes := MyB(str)
	fmt.Printf("String -> runes slice:\n% x\n\n", myrunes)
	fmt.Println("Iterate over runes slice (for-range):")
	for _, v := range myrunes {
		fmt.Printf("%c(%x) ", v, v)
	}
	fmt.Printf("\n\n")
	fmt.Printf("String -> bytes slice:\n% x\n\n", mybytes)
	fmt.Println("Iterate over bytes slice (for-range):")
	for _, v := range mybytes {
		fmt.Printf("%d(%x) ", v, v)
	}
	fmt.Printf("\n\n")
	fmt.Printf("Get symbols by index (from runes!): %c %c %c\n", myrunes[0], myrunes[1], myrunes[2])

	r1, size1 := utf8.DecodeLastRune(mybytes)
	fmt.Printf("DecodeLastRune: rune=%c len=%d\n", r1, size1)

	r2, size2 := utf8.DecodeRune(mybytes)
	fmt.Printf("DecodeRune: rune=%c len=%d\n\n", r2, size2)

	//decode all runes in string
	fmt.Printf("Decode all runes in string (DecodeRuneInString):\n")
	copyStr1 := str
	for len(copyStr1) > 0 {
		r, size := utf8.DecodeRuneInString(copyStr1)
		fmt.Printf("%c(%x)(size=%d),", r, r, size)
		copyStr1 = copyStr1[size:]
	}
	fmt.Printf("\n\n")

	//changing one character in a string
	fmt.Printf("Changing one character in a string (by rune slice!):\n")
	sss := "СорокиЫ"
	r := []rune(sss)
	r[6] = 'н'
	fmt.Printf("%s -> %s:\n", sss, string(r))
}
