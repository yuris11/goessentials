package main

import (

	// "bitbucket.org/yuris11/goessentials/src/arrays"
	// "bitbucket.org/yuris11/goessentials/src/http"

	"bitbucket.org/yuris11/goessentials/src/interfaces"
	//"bitbucket.org/yuris11/goessentials/src/time1"
	// "bitbucket.org/yuris11/goessentials/src/methods"
	// "bitbucket.org/yuris11/goessentials/src/slices"
	// "bitbucket.org/yuris11/goessentials/src/values"
	// "bitbucket.org/yuris11/goessentials/src/flags"
	// "bitbucket.org/yuris11/goessentials/src/toml"
	"bitbucket.org/yuris11/goessentials/src/strings"
	// "bitbucket.org/yuris11/goessentials/src/goroutines"
)

func main() {
	//goroutines.Conveyuor()
	//goroutines.Channels001()
	//goroutines.Mutex()
	//goroutines.Channels()
	//goroutines.Factorial()
	//goroutines.Factorial1()
	// flags.Flags()
	// toml.Toml()
	// values.Values()
	// values.Constants()
	// arrays.Arrays()
	// methods.Methods()
	// slices.Slices()
	//interfaces.Interfaces()
	interfaces.InterfEmpty()
	//maps.Maps()
	strings.Strings()
	// http.Http()
	// context1.Cont2()
	//time1.Time1()
}
