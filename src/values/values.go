package values

import "fmt"

//Values ...
func Values() {
	fmt.Println("--------values--------------")
	var aa, aa1 int
	aa = 123
	bb := 456
	ff := 3.1415
	aa1 = int(ff)
	var b1 bool = true
	b1 = false
	ss := "Это строка"
	fmt.Println(aa, aa1, bb, b1, ss, ff)

}
