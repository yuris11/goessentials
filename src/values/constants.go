package values

import "fmt"

//Constants ...
func Constants() {
	fmt.Println("---------values - Constants--------")
	const c = 3.12
	const (
		a = 1
		b = 2
		c1
		d1 = 6
	)
	fmt.Println(c, a, b, c1, d1)
}
