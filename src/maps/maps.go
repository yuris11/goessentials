package maps

import "fmt"

type mymap map[int]string

//Maps ...
func Maps() {
	fmt.Println("----------maps-------------")
	m1 := map[string]int{
		"Sorokin": 62,
		"Moshkin": 59,
	}
	fmt.Printf("%v\n",m1)
	if age, ok := m1["Sorokin"]; ok {
		fmt.Printf("Age=%v\n\n", age)
	} else {
		fmt.Printf("---ERROR---\n\n")
	}

	m2 := mymap{
		1: "Sorokin",
		2: "Moshkin",
		3: "Buchnew",
	}
	m2[4] = "Popov"

	for key, v := range m2 {
		fmt.Println(key, v)
	}
	fmt.Println("")

	m3 := make(mymap, 10)
	m3[4] = "Moscow"
	for key, val := range m3 {
		fmt.Println(key, val)
	}

}
